<?php get_header(); ?>

<?php 
// THE LOOP
if ( have_posts() ) :
	while ( have_posts() ) : the_post(); ?>

  <h1><?php the_title(); ?></h1>

  <div><small><?php the_author(); ?> - <?php the_date(); ?></small></div>
  <div><em><?php the_category( ' | ' ); ?></em></div>
  
  <?php if ( has_post_thumbnail() ) : ?>
		<?php the_post_thumbnail( ); ?>
	<?php endif; ?>

  <?php the_content(); ?>
  
<?php 
	endwhile;
endif;
?>

<?php get_footer(); ?>