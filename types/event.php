<?php 

// AJOUT D'UN CUTSOM POST TYPE

//options
$args = array(
  'labels' => array(
    'name' => 'Evénements',
    'singular_name' => 'Evénement',
  ),
  'public' => true,
  'publicly_queryable' => true,
  'show_ui' => true,
  'show_in_menu' => true,
  'query_var' => true,
  'rewrite' => array( 'slug' => "evenements"),
  'capability_type' => 'post',
  'has_archive' => true,
  'hierarchical' => false,
  'menu_position' => 4,
  'supports' => array('title','editor','thumbnail','custom-fields'),
);
register_post_type('my_event',$args);


// AJOUT D'UNE NOUVELLE TAXONOMIE
register_taxonomy(
    'events_category',
    'my_event',
    array(
      'label' => 'Catégorie d\'événements',
      'hierarchical' => true) 
  );


?>