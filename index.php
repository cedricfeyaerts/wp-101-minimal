<?php get_header(); ?>

<?php 
// THE LOOP
if ( have_posts() ) :
	while ( have_posts() ) : the_post(); ?>

  <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

  <div><small><?php the_author(); ?> - <?php the_date(); ?></small></div>
  <div><em><?php the_category( ' | ' ); ?></em></div>

  <?php the_excerpt(); ?>
  
<?php 
	endwhile;
endif;
?>

<?php get_footer(); ?>