<?php 
/**
 * Quelques snippets utiles a mettre dans functions.php 
 *
 */


//--------------------------------------------------------------

// inclure un autre fichier contenant des fonctions
require TEMPLATEPATH.'/includes/my_other_file.php';
foreach(glob(TEMPLATEPATH.'/types/*.php') as $file){
    require $file;
}

//--------------------------------------------------------------


//charger des script et des styles
function load_my_scripts() {

  //js
  wp_enqueue_script( 'my_script', get_template_directory_uri().'/assets/js/my_script.js', array('jquery'), '1.0.0' ); // ce script a besoin de jquery et sera donc charger apres
  wp_enqueue_script( 'my_second_script', get_template_directory_uri().'/assets/js/my_second_script.js', array(), '1.0.0' ); // ce script n'a besoin de rien
  wp_enqueue_script( 'my_third_script', get_template_directory_uri().'/assets/js/my_third_script.js', array('my_script'), '1.0.0', true ); // ce script a besoin de my_script et sera charger dans le footer

  //css
  wp_enqueue_style( 'style.css', get_stylesheet_uri(), array(), '1.0.0' );


  // seulement si la page ou le post a un template 'My Page'
  if( is_page_template('templates/my_page.php') ):
    wp_enqueue_script( 'my_page_script', get_template_directory_uri() . '/assets/js/my_page_script.js', array( 'jquery' ), '20150330', true );
  endif;
}
add_action('wp_enqueue_scripts', 'load_my_scripts');

//--------------------------------------------------------------

// AJOUT UNE TAILLE DE THUMBNAIL
add_theme_support('post-thumbnails');
add_image_size( 'custom', 300, 300, true );

// ajoute des emplacement de menu
register_nav_menus( array(
  'mon_menu' => 'Mon menu',
  'mon_autre_menu' => 'Mon autre menu',
) );

//--------------------------------------------------------------

//add your css to the editor
add_editor_style( 'assets/styles/editor_style.css' );

//--------------------------------------------------------------

// ajoute des styles dans l'editeur de texte
//ajoute le bouton
function my_mce_buttons_2( $buttons ) {
  array_unshift( $buttons, 'styleselect' );
  return $buttons;
}
add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );
//ajoute les styles
function ajout_de_style_mce( $init_array ) {  
  
  $style_formats = array(  

    array( 
      'title' => 'Mon span', 
      'inline' => 'span', 
      'classes' => 'mon-span',
      'wrapper' => true,
    ),
    array(  
      'title' => 'mon paragraphe',  
      'block' => 'p', 
      'classes' => 'mon-p', 
      'wrapper' => false, 
    ),

  );  
  
  $init_array['style_formats'] = json_encode( $style_formats );  
  
  return $init_array;  
  
} 
add_filter( 'tiny_mce_before_init', 'ajout_de_style_mce' );  

//--------------------------------------------------------------

//un example de shortcode avec attribut par defaut
// [foobar foo="bar"]
function foo_shortcode_func( $atts ) {
    $a = shortcode_atts( array(
        'foo' => 'bar',
    ), $atts );
    
    return 'foo'.$a['foo'];
}
add_shortcode( 'foobar', 'foo_shortcode_func' );

//--------------------------------------------------------------

// charger les traductions

function my_child_theme_setup() {
            load_child_theme_textdomain( 'wp-101-minimal', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'my_child_theme_setup' );
?>