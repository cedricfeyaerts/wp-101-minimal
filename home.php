<h1> home </h1>
<?php get_header(); ?>

<h2> Woocommerce shortcode <a href="https://docs.woocommerce.com/document/woocommerce-shortcodes/">Doc woocommerce</a></h2> 
<?php echo do_shortcode( '[recent_products orderby="date" order="desc" per_page="3" columns="3"]' )?>


<h2> Custom query </h2>

<ul class="products">
	<?php
		$args = array(
			'post_type' => 'product',
			'posts_per_page' => 12
			);
		$loop = new WP_Query( $args );
		if ( $loop->have_posts() ) {
			while ( $loop->have_posts() ) : $loop->the_post();?>
                <h3><?php the_title(); ?></h3>
                <p><?php the_excerpt(); ?></p>
                <p><?php echo $product->get_price_html(); ?></p>
                <p></p>
			<?php endwhile;
		}
		wp_reset_postdata();
	?>
</ul>
<?php get_footer(); ?>